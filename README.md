To run the server, run "node server/main.js", and open the client/index.html in Chrome. 

You might want to open multiple instances of the client in different tabs, and explore having multiple player in the game at the same time.

Controls:
Arrows = Move, 
K = Pick up Object, 
Click Object in UI = Drop Object

Everything takes an action, including moving, picking stuff up, dropping things, and chatting.